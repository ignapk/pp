#include <stdio.h>
#include <math.h>

int main()
{
	float a, b, c, d;
	scanf("%f%f%f", &a, &b, &c);
	d = b * b - 4 * a * c;
	if (d >= 0)
		printf("x1: %f, x2: %f", (-1 * b - sqrt(d))/(2 * a), (-1 * b + sqrt(d))/(2 * a));
	else
		printf("brak");
	return 0;
}
