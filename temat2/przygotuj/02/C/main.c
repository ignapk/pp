#include <stdio.h>

int main()
{
	float a, b;
	scanf("%f%f", &a, &b);
	if (b == 0)
		printf("Cannot divide by zero");
	else
		printf("%f", a / b);
	return 0;
}
