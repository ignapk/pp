#include <stdio.h>

int main()
{
	double a, b, temp;
	scanf("%lf%lf", &a, &b);
	printf("a = %lf\nb = %lf\n", a, b);
	temp = a;
	a = b;
	b = temp;
	printf("a = %lf\nb = %lf\n", a, b);
	return 0;
}
