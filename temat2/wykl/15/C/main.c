#include <stdio.h>
#include <stdbool.h>

int main()
{
	bool b1 = true, b2 = false;
	int v1 = 0, v2 = 123, v3 = -555;
	bool result = b1 + b2;
	if(result)
		printf("Suma wyrazen logicznych b1 i b2: %s\n", result ? "true" : "false");
	else
		printf("Cos tam\n");
	return 0;
}

