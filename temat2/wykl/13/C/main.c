#include <stdio.h>

int main()
{
	float a = 1.f / 10.f, b = 1.f - 0.9f;
	printf("%.8f %.8f\n", a, b);

	if (a == b)
	{
		printf("Zgadza się!");
	}
	else
	{
		printf("Nie zgadza się!");
	}
	return 0;
}
