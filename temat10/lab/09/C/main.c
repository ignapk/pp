#include <stdio.h>
#include <stdlib.h>

float **fun (float *arr, int n, int m)
{
	float **ret = malloc (sizeof (float *) * n);
	for (int i = 0; i < n; i++)
	{
		*(ret + i) = malloc (sizeof (float) * m);
		for (int j = 0; j < m; j++)
			if (i == 0 || i == n - 1 || j == 0 || j == m - 1)
				*(*(ret + i) + j) = 0.f;
			else
				*(*(ret + i) + j) = *(arr + i * m + j);
	}
	return ret;
}

int main ()
{
	const int n = 3, m = 5;
	float arr[] = {1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f};
	float **arr2d = fun (arr, n, m);
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
			printf ("%f ", *(*(arr2d + i) + j));
		printf ("\n");
		free (*(arr2d + i));
	}
	free (arr2d);
	return 0;
}
