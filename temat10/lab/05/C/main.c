#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int comp (const void *a, const void *b)
{
	return strcmp ((const char *)b, (const char *)a);
}

int main ()
{
	char arr[5][100] = {"tygrys syberyjski", "krokodyl nilowy", "panda wielka", "tygrys azjatycki","chomik europejski"};
	qsort ((void *)arr, 5, 100, &comp);
	for (int i = 0; i < 5; i++)
		printf ("%s\n", *(arr + i));

	return 0;
}
