#include <stdio.h>

int add (int a, int b)
{
	return a + b;
}

int sub (int a, int b)
{
	return a - b;
}

int main ()
{
	int (*fptr) (int, int);
	fptr = &add; //same as fptr = add
	printf ("%d\n", (*fptr) (2, 2)); //same as fptr (2, 2)
	fptr = &sub;
	printf ("%d\n", (*fptr) (2, 2));
	return 0;
}
