#include <stdio.h>
#include <stdlib.h>

void fun (int *arr, int n)
{
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			*(arr + i * n + j) = (i + 1) * (j + 1);
}

int main ()
{
	int n = 10;
	int *arr = malloc (sizeof (int) * n * n);
	fun (arr, n);
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			printf("%d\t", *(arr + i * n + j));
		printf ("\n");
	}
	free (arr);
	return 0;
}
