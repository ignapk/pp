#include <stdio.h>
#include <stdlib.h>

int smaller (const void *a, const void *b)
{
	return *(float *)b - *(float *)a;
}

int bigger (const void *a, const void *b)
{
	return *(int *)a - *(int *)b;
}

int main ()
{
	float arr1[] = {1.f, 2.f, 3.f, 5.f, 15.f, 0.f, 12.f, 13.f, 5.f, 7.f};
	int arr2[] = {1, 2, 3, 5, 15, 0, 12, 13, 5, 7};
	qsort ((void *)arr1, 10, sizeof (float), &smaller);
	qsort ((void *)arr2, 10, sizeof (int), &bigger);
	for (int i = 0; i < 10; i++)
		printf ("%f ", *(arr1 + i));
	for (int i = 0; i < 10; i++)
		printf ("%i ", *(arr2 + i));

	return 0;
}
