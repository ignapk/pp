#include <stdio.h>

int maxi (int a, int b)
{
	//return (a > b) ? 1 : 0;
	return a > b;
}

int mini (int a, int b)
{
	//return (a < b) ? 1 : 0;
	return a < b;
}

int *f (int *arr, int n, int (*cmp) (int a, int b))
{
	int *ret = arr;
	for (int i = 1; i < n; i++)
		if ((*cmp) (*(arr + i), *ret))
			ret = arr + i;
	return ret;
}

int main ()
{
	int arr[] = {1, 2, 3, 5, 15, 0, 12, 13, 5, 7};
	printf ("%d", *f (arr, 10, &maxi));
	printf ("%d", *f (arr, 10, &mini));
	return 0;
}
