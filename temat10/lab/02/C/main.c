#include <stdio.h>

float add (float a, float b)
{
	return a + b;
}

float sub (float a, float b)
{
	return a - b;
}

float mul (float a, float b)
{
	return a * b;
}

float div (float a, float b)
{
	return a / b;
}

int main ()
{
	float a, b;
	scanf ("%f %f", &a, &b);
	int o;
	scanf ("%d", &o);
	float (*p[4]) (float, float) = {&add, &sub, &mul, &div};
	printf ("%f", (*p[o]) (a, b));
	return 0;
}
	
