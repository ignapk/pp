#include <stdio.h>
#include <stdlib.h>

int fun (int *arr, int begin, int end, int value)
{
	int half = begin + (end - begin - 1)/2;
	if (half > end || half < begin)
		return -1;
	else if (arr[half] == value)
		return half;
	else if (arr[half] > value)
		return fun (arr, begin, half - 1, value);
	else
		return fun (arr, half + 1, end, value);
}

int comp (const void *a, const void *b)
{
	return *(int *)a - *(int *)b;
}

int main ()
{
	int v;
	scanf ("%d", &v);
	int arr[] = {10, 2, 3, 45, 10, 22, 23, 42};
	qsort (arr, 10, sizeof (int), &comp);
	for (int i = 0; i < 10; i++)
		printf ("%d ", *(arr + i));
	printf ("\n");
	printf ("%d", fun (arr, 0, 9, v));
	printf ("%d", (int *)bsearch (&v, arr, 10, sizeof (int), &comp) - arr);
	return 0;
}
