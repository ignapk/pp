#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void fill (int *arr, int n, int x)
{
	for (int i = 0; i < n; i++)
		*(arr + i) = x + i;
}

int randcmp (const void *a, const void *b)
{
	return rand () % 2 ? 1 : -1;
}

void print (int *arr, int n)
{
	for (int i = 0; i < n; i++)
		printf ("%d ", *(arr + i));
	printf ("\n");
}

int sum (int *arr, int n)
{
	int ret = 0;
	for (int i = 0; i < n; i++)
		ret += *(arr + i);
	return ret;
}

int dot (int *u, int *v, int n)
{
	int ret = 0;
	for (int i = 0; i < n; i++)
		ret += *(u + i) * *(v + i);
	return ret;
}

int main ()
{
	int n, x;
	scanf ("%d%d", &n, &x);
	int *arr = malloc (sizeof (int) * n);
	int *brr = malloc (sizeof (int) * n);
	fill (arr, n, x);
	fill (brr, n, x);
	qsort (arr, n, sizeof (int), &randcmp);
	qsort (brr, n, sizeof (int), &randcmp);
	print (arr, n);
	print (brr, n);
	printf ("%d\n", sum (arr, n));
	printf ("%d\n", sum (brr, n));
	printf ("%d\n", dot (arr, brr, n));
	free (arr);
	free (brr);
	return 0;
}
