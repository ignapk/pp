#include <stdio.h>
#include <stdlib.h>

float *fun (float *arr, int n, float min, float max, int *m)
{
	float *ret;
	*m = 0;
	for (int i = 0; i < n; i++)
		if (*(arr+i) >= min && *(arr+i) < max)
			(*m)++;

	ret = malloc (sizeof (float) * *m);
	
	for (int i = 0, j = 0; i < n; i++)
		if (*(arr+i) >= min && *(arr+i) < max)
			*(ret + j++) = *(arr + i);

	return ret;
}

int main ()
{
	int n = 10;
	float arr[] = {0.5f, 0.7f, 1.25f, 6.25f, 9.3f, 1.13f, 2.23f, 3.2f, 6.23f, 1.78f};
	int *m = malloc (sizeof (int));
	float *ret = fun (arr, n, 1.13f, 6.3f, m);
	for (int i = 0; i < *m; i++)
		printf("%f ", *(ret + i));
	free (ret);
	free (m);
	return 0;
}
