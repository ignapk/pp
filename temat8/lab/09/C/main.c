#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void init_arr (int *arr, int n)
{
	for (int i = 0; i < n; i++)
		*(arr + i) = rand () % 6;
}

void sort_arr (int *arr, int n)
{
	int temp;
	for (int i = 0; i < n - 1; i++)
		for (int j = 0; j < n - i - 1; j++)
			if (*(arr + j) > *(arr + j + 1))
			{
				temp = *(arr + j);
				*(arr + j) = *(arr + j + 1);
				*(arr + j + 1) = temp;
			}
}

void print_arr (int *arr, int n)
{
	for (int i = 0; i < n; i++)
		printf("%d ", *(arr + i));
	printf("\n");
}

void show_common (int *narr, int n, int *marr, int m, int *oarr, int o)
{
	int i = 0, j = 0, k = 0;
	while (i < n && j < m && k < o)
	{
		if (*(narr + i) == *(marr + j) && *(marr + j) == *(oarr + k))
		{
			printf ("%d\n", *(narr + i));
			i++, j++, k++;
		}
		else if (*(narr + i) < *(marr + j))
			i++;
		else if (*(marr + j) < *(oarr + k))
			j++;
		else
			k++;
	}
}

int main ()
{
	int n, m, o;
	srand (time (0));
	scanf ("%d %d %d", &n, &m, &o);
	int *narr = malloc (sizeof (int) * n);
	int *marr = malloc (sizeof (int) * m);
	int *oarr = malloc (sizeof (int) * o);
	
	init_arr (narr, n);
	init_arr (marr, m);
	init_arr (oarr, o);
	sort_arr (narr, n);
	sort_arr (marr, m);
	sort_arr (oarr, o);
	print_arr (narr, n);
	print_arr (marr, m);
	print_arr (oarr, o);
	show_common (narr, n, marr, m, oarr, o);

	free (narr);
	free (marr);
	free (oarr);
	return 0;
}
