#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void init_arr (float *arr, int n)
{
	for (int i = 0; i < n; i++)
		*(arr + i) = 2.5f + rand () * 2.9f / RAND_MAX;
}

void print_arr (float *arr, int n)
{
	for (int i = 0; i < n; i++)
		printf ("%f ", *(arr + i));
	printf ("\n");
}

float sum_arr (float *arr, int n)
{
	float sum = 0.f;
	for (int i = 0; i < n; i++)
		sum += *(arr + i);
	return sum;
}

int main ()
{
	srand (time (0));
	int n, m, o;
	scanf ("%d %d %d", &n, &m, &o);
	float **arr = malloc (sizeof (float *) * 3);	
	*arr = malloc (sizeof (float) * n);
	*(arr + 1) = malloc (sizeof (float) * m);
	*(arr + 2) = malloc (sizeof (float) * o);

	init_arr (*arr, n);
	init_arr (*(arr + 1), m);
	init_arr (*(arr + 2), o);

	print_arr (*arr, n);
	print_arr (*(arr + 1), m);
	print_arr (*(arr + 2), o);	

	float nsum = sum_arr (*arr, n);
	float msum = sum_arr (*(arr + 1), m);
	float osum = sum_arr (*(arr + 2), o);

	printf("%f\n%f\n%f\n", nsum, msum, osum);

	if (nsum > msum && nsum > osum)
		print_arr (*arr, n);
	else if (msum > osum)
		print_arr (*(arr + 1), m);
	else
		print_arr (*(arr + 2), o);

	free (*arr);
	free (*(arr + 1));
	free (*(arr + 2));
	free (arr);
	return 0;
}
