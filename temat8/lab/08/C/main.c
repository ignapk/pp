#include <stdio.h>
#include <stdlib.h>

void f1 (int **arr, int n, int m)
{
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			*(*(arr + i) + j) = i * j;
}

void f2 (int **arr, int n, int m)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
			printf ("%d ", *(*(arr + i) + j));
		printf ("\n");
	}
}

int main ()
{
	int n, m;
	scanf ("%d %d", &n, &m);
	int **arr = malloc (sizeof (int *) * n);
	for (int i = 0; i < n; i++)
		*(arr + i) = malloc (sizeof (int) * n);

	f1 (arr, n, m);
	f2 (arr, n, m);

	for (int i = 0; i < n; i++)
		free (*(arr + i));
	free (arr);
	return 0;
}
