#include <stdio.h>

void swap (void *a, void *b, int n)
{
	char temp;
	char *a_c = (char *)a;
	char *b_c = (char *)b;
	for (int i = 0; i < n; i++)
	{
		temp = *(a_c + i);
		*(a_c + i) = *(b_c + i);
		*(b_c + i) = temp;
	}
}

int main ()
{
	int a = 3, b = 4;
	float c = 1.6, d = 1.8;
	char e = 'e', f = 'f';

	swap (&a, &b, sizeof (int));
	swap (&c, &d, sizeof (float));
	swap (&e, &f, sizeof (char));

	printf ("%d %d %f %f %c %c", a, b, c, d, e, f);
	return 0;
}
