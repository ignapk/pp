#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main ()
{
	int n, m, tmp, count=0;
	srand (time (0));
	scanf ("%d %d", &n, &m);
	int *arr = malloc (sizeof (int) * n);
	for (int i = 0; i < n; i++)
		*(arr + i) = -50 + rand () % 101;
	printf ("\n");
	for (int i = 0; i < n - 1; i++)
		for (int j = 0; j < n - i - 1; j++)
			if (*(arr + j) > *(arr + j + 1))
			{
				tmp = *(arr + j);
				*(arr + j) = *(arr + j + 1);
				*(arr + j + 1) = tmp;
			}
	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
			if (*(arr + i) + *(arr + j) == m)
			{
				printf ("(%d, %d)", *(arr + i), *(arr + j));
				count++;
			}
	printf("\n%d\n", count);
	free (arr);
	return 0;
}
