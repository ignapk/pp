#include <stdio.h>

int *fun (int *a, int *b)
{
	return *a > *b ? b : a;
}

int main ()
{
	int a = 7, b = 6;
	int *ret = fun (&a, &b);
	printf("%d %d %d\n", *ret, a, b);
	return 0;
}
