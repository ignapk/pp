#include <stdio.h>
#include <stdlib.h>

int main ()
{
	unsigned int n;
	scanf ("%u", &n);
	float *arr = malloc (sizeof (float) * n);
	for (int i = 0; i < n; i++)
		*(arr + i) = 0;
	for (int i = 0; i <n; i++)
		printf ("%f ", *(arr + i));
	printf ("\n");
	free (arr);
	arr = malloc (sizeof (float) * 10);
	for (int i = 0; i < 10; i++)
		*(arr + i) = 1;
	for (int i = 0; i < 10; i++)
		printf ("%f ", *(arr + i));
	printf ("\n");
	free (arr);
	return 0;
}
