#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void fun (float *arr1, int n, int *arr2, int m)
{
	for (int i = 0; i < m; i++)
		if (*(arr2 + i) >= n || *(arr2 + i) < 0)
			printf ("%f ", NAN);
		else
			printf ("%f ", *(arr1 + *(arr2 + i)));
}

int main ()
{
	int n, m;
	scanf ("%d %d", &n, &m);
	float *arr1 = malloc (sizeof (float) * n);
	int *arr2 = malloc (sizeof (int) * m);
	for (int i = 0; i < n; i++)
		*(arr1 + i) = i*i;
	for (int i = 0; i < m; i++)
		scanf("%d", arr2 + i);
	fun (arr1, n, arr2, m);
	free (arr1);
	free (arr2);
	return 0;
}

