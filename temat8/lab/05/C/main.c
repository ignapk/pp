#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void f1 (float *arr, int n)
{
	for (int i = 0; i < n; i++)
		*(arr + i) = i + 1;
	return;
}

void f2 (int n, float *arr)
{
	float prev = 0;
	for (int i = 1; i <= n; i++)
	{
		prev += pow(-1, i) * *(arr + i - 1);
		printf ("%f\n", prev);
	}
}

int main ()
{
	int n;
	scanf ("%d", &n);
	float *arr = malloc (sizeof (float) * n);

	f1 (arr, n);
	f2 (n, arr);

	free (arr);

	return 0;
}
