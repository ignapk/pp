#include <stdio.h>

int main()
{
	int value, sum = 0;
	do {
		scanf("%d", &value);
		sum += value;
	} while (value);
	printf("%d", sum);
	return 0;
}
