#include <stdio.h>

int main()
{
	int a, c =0;
	scanf("%d", &a);	
	if(a == 0) c = 1;
	else
		while(a)
		{
			c++;
			a /= 10;
		}
	printf("%d", c);
	return 0;
}
