#include <stdio.h>
#include <stdlib.h>

int main()
{
	int a, b, counter = 0;
	scanf("<%d;%d>", &a, &b);
	for(int i = a; i <= b; i++)
	{
		int tmp = abs(i);
		int found = 0;
		while(tmp)
		{
			if (tmp%10 == 5)
			{
				found = 1;
				break;
			}
			tmp /= 10;
		}
		if(found)
			continue;
		printf("%d ", i);
		counter++;
	}
	printf("%d <%d;%d>", counter, a, b);
	return 0;
}
