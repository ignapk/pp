#include <stdio.h>
int main()
{
	float a, b, c, d;
	scanf("%f%f%f%f", &a, &b, &c, &d);
	// <a;b> <c;d>
	if (d < a || c > b)
		printf("no intersection");
	else
		printf("intersection");
	return 0;
}
