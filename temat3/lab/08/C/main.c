#include <stdio.h>

int main()
{
	int year;
	scanf("%d", &year);
	if (!(year % 400) || (!(year % 4) && (year % 100 !=0)))
		printf("leap year");
	else
		printf("common year");
	return 0;
}
