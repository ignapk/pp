#include <cstdio>
#include <cmath>

class Point
{
	public:
		float x, y;
};

void f (Point *arr, int n, float a, float b, float r, Point **result)
{
	Point p;
	for (int i = 0, j = 0; i < n; i++)
	{
		p = *(arr + i);
		if (pow (p.x - a, 2) + pow (p.y - b, 2) <= pow (r, 2))
			*(result + j++) = arr + i;
	}
}

int main ()
{
	Point arr[5] = {0.2, 0.3f, 1.f, 1.5f, 2.f, 2.2f, 0.1f, 0.1f, 3.f, 3.2f};
	int count = 0;
	for (int i = 0; i < 5; i++)
		if (pow (arr[i].x - 0, 2) + pow (arr[i].y - 0, 2) <= pow (1, 2))
			count++;
	Point **result = (Point **)malloc (sizeof (Point *) * count);
	f (arr, 5, 0.f, 0.f, 1.f, result);
	for (int i = 0; i < count; i++)
		printf ("(%f:%f)\n", result[i]->x, result[i]->y);
	free (result);
	return 0;
}
