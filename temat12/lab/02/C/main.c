#include <stdio.h>

enum direction {LEFT, RIGHT, UP, DOWN};

int main ()
{
	int i;
	scanf ("%i", &i);
	switch (i)
	{
		case LEFT:
			printf ("Left");
			break;
		case RIGHT:
			printf ("Right");
			break;
		case UP:
			printf ("Up");
			break;
		case DOWN:
			printf ("Down");
			break;
		default:
			printf ("Anywhere");
			break;
	}
	return 0;
}
