#include <stdio.h>
#include <math.h>

struct point
{
	float x;
	float y;
};

float distance (struct point a, struct point b)
{
	return sqrt (pow (a.x - b.x, 2) + pow (a.y - b.y, 2));
}

float perimeter (struct point *arr, int n)
{
	float ret = 0.f;
	for (int i = 0; i < n; i++)
		ret += distance (*(arr + i), *(arr + (i + 1) % n));
	return ret;
}

int main ()
{
	struct point arr[4];
	for (int i = 0; i < 4; i++)
	{
		arr[i].x = i;
		arr[i].y = 4 - i;
	}
	printf ("%f", perimeter (arr, 4));
	return 0;
}
