#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct person
{
	int age;
	char *name;
};

int main ()
{
	struct person me;
	me.age = 22;
	me.name = malloc (sizeof (char) * 24);
	strcpy (me.name, "Ignacy");
	printf ("%d%s", me.age, me.name);
	struct person clone = me;
	clone.name = malloc (sizeof (char) * 24);
	strcpy (clone.name, me.name);
	free (me.name);
	printf ("%d%s", clone.age, clone.name);
	free (clone.name);
	return 0;
}
