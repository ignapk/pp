#include <stdio.h>
#include <math.h>

struct point
{
	float x;
	float y;
};

int main ()
{
	struct point p1;
	struct point p2;
	scanf ("%f%f", &p1.x, &p1.y);
	scanf ("%f%f", &p2.x, &p2.y);
	printf ("%f", sqrtf (pow (p1.x - p2.x, 2) + pow (p1.y - p2.y, 2)));
	return 0;
}
