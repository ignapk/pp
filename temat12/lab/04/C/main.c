#include <stdio.h>

enum type {CHAR, UCHAR, INT, FLOAT};

float fun (void *arr, int n, enum type t)
{
	float ret = 0.f;
	switch (t)
	{
		case CHAR:
			char *castc = (char *)arr;
for (int i = 0; i < n; i++)
		ret += *(castc + i)/n;

			break;
		case UCHAR:
			unsigned char *castu = (unsigned char *)arr;
for (int i = 0; i < n; i++)
		ret += *(castu + i)/n;

			break;
		case INT:
			int *casti = (int *)arr;
for (int i = 0; i < n; i++)
		ret += *(casti + i)/n;

			break;
		case FLOAT:
			float *castf = (float *)arr;
for (int i = 0; i < n; i++)
		ret += *(castf + i)/n;

			break;
		default:
			return 0.f;
			break;
	}
		return ret;
}

int main ()
{
	unsigned char arr1[5] = {50, 100, 150, 200, 250};
	float arr2[5] = {50, 100, 150, 200, 250};
	printf ("%f%f", fun (arr1, 5, UCHAR), fun (arr2, 5, FLOAT));
	return 0;
}
