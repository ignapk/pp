#include <stdio.h>
#include <stdlib.h>

struct node
{
	void *data;
	struct node *next;
};

void push_front (struct node **head_ref, void *new_data, size_t data_size)
{
	struct node *new_node = malloc (sizeof (struct node));
	new_node->data = malloc (sizeof (char) * data_size);
	for (size_t i = 0; i < data_size; i++)
		*((char *)new_node->data + i) = *((char *)new_data + i);

	new_node->next = *head_ref;
	*head_ref = new_node;
}

void print_list (struct node *node, void (*fptr) (void *))
{
	while (node != NULL)
	{
		(*fptr) (node->data);
		node = node->next;
	}
	printf ("\n");
}

void free_list_data (struct node *node)
{
	while (node != NULL)
	{
		free (node->data);
		node = node->next;
	}
}

void free_list (struct node *head_ref)
{
	if (head_ref == NULL)
		return;
	free_list (head_ref->next);
	free (head_ref);
}

void print_int (void *data)
{
	printf ("%i ", *(int *)data);
}

void print_float (void *data)
{
	printf ("%f ", *(float *)data);
}

int main ()
{
	struct node *start = NULL;
	unsigned int int_size = sizeof (int);
	int arr1[] = {10, 20, 30, 40, 50}, i;
	for (i = 4; i >= 0; i--)
		push_front (&start, &arr1[i], int_size);
	print_list (start, print_int);

	free_list_data (start);
	free_list (start);
	start = NULL;
	unsigned int float_size = sizeof (float);
	float arr2[] = {10.1f, 20.2f, 30.3f, 40.4f, 50.5f};
	for (i = 4; i >= 0; --i)
		push_front (&start, &arr2[i], float_size);
	print_list (start, print_float);

	free_list_data (start);
	free_list (start);
	return 0;
}
