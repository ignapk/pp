#include <stdio.h>
#include <stdlib.h>

struct vector
{
	float x;
	float y;
	float z;
};

struct vector *add (struct vector *v1, struct vector *v2)
{
	struct vector *ret = malloc (sizeof (struct vector));
	ret->x = v1->x + v2->x;
	ret->y = v1->y + v2->y;
	ret->z = v1->z + v2->z;
	return ret;
}

struct vector *diff (struct vector *v1, struct vector *v2)
{
	struct vector *ret = malloc (sizeof (struct vector));
	ret->x = v1->x - v2->x;
	ret->y = v1->y - v2->y;
	ret->z = v1->z - v2->z;
	return ret;
}



int main ()
{
	struct vector v1 = {3, 3, 3};
	struct vector v2 = {1, 1, 1};
	struct vector **result = malloc (sizeof (struct vector *) * 2);
	*result = add (&v1, &v2);
	*(result + 1) = diff (&v1, &v2);
	printf ("%f%f%f", (*result)->x, (*result)->y, (*result)->z);
	printf ("%f%f%f", (*(result + 1))->x, (*(result + 1))->y, (*(result + 1))->z);
	free (*result);
	free (*(result + 1));
	free (result);
	return 0;
}
