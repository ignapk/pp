#include <cstdio>
#include <cmath>

class Point
{
	public:
		float x, y;
};

float distance (Point p)
{
	return sqrt (pow (p.x, 2) + pow (p.y, 2));
}

void f (Point *arr, int n, Point **ret)
{
	float dist = distance (*arr);
	int id = 0;
	for (int i = 1; i < n; i++)
		if (distance (*(arr + i)) < dist)
			dist = distance (*(arr + i)), id = 1;
	*ret = arr + id;
}

int main ()
{
	Point arr[5] = {0.2, 0.3f, 1.f, 1.5f, 2.f, 2.2f, 0.1f, 0.1f, 3.f, 3.2f};
	Point *p;
	f (arr, 5, &p);
	printf ("(%f:%f)\n", p->x, p->y);
	return 0;
}
