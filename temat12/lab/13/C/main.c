#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void sort_ships (float *ships, char **ship_names, int n)
{
	float temp1, temp2;
	char temp3[100];
	for (int i = 0; i < n - 2; i+=2)
	{
		for (int j = 0; j < n - i - 2; j+=2)
		{
			if (*(ships + j) > *(ships + j + 2) || (*(ships + j) == *(ships + j + 2) && *(ships + j + 1) > *(ships + j + 3)))
			{
				temp1 = *(ships + j);
				temp2 = *(ships + j + 1);
				*(ships + j) = *(ships + j + 2);
				*(ships + j + 1) = *(ships + j + 3);
				*(ships + j + 2) = temp1;
				*(ships + j + 3) = temp2;
				strcpy (temp3, *(ship_names + j/2));
				strcpy (*(ship_names + j/2), *(ship_names + j/2 +1));
				strcpy (*(ship_names + j/2 + 1), temp3);
			}
		}
	}
}


int main ()
{
	//const int N = 3;
	int N;
	scanf ("%d", &N);
	float *ships = malloc (sizeof (float) * 2 * N);
	for (int i = 0; i < 2 * N; i++)
		scanf ("%f", ships + i);
	char **ship_names = malloc (sizeof (char *) * N);
	for (int i = 0; i < N; i++)
	{
		*(ship_names + i) = malloc (sizeof (char) * 100);
		scanf ("%s", *(ship_names + i));
	}
	//float ships[] = {1.2f, 6.1f, 1.2f, 1.1f, 5.4f, 3.1f};
	//char ship_names[][100] = {"Alfa", "Omega", "Sigma"};
	sort_ships (ships, ship_names, 2 * N);
	for (int i = 0; i < 3; i++)
		printf ("%s\n", *(ship_names + i));
	free (ships);
	for (int i = 0; i < N; i++)
		free (*(ship_names + i));
	free (ship_names);
	return 0;
}
