#include <stdio.h>
#include <complex.h>

int main ()
{
	double complex z1 = 1 + 3 * I;
	double complex z2 = 1 + -4 * I;

	double complex sum = z1 + z2;
	double complex difference = z1 - z2;
	double complex product = z1 * z2;
	double complex quotient = z1 / z2;
	double complex conjugate = conj (z1);

	printf ("z1 = %f + %fi\n", z1);
	printf ("z2 = %f + %fi\n", z2);
	printf ("sum = %f + %fi\n", sum);
	printf ("difference = %f + %fi\n", difference);
	printf ("product = %f + %fi\n", product);
	printf ("quotient = %f + %fi\n", quotient);
	printf ("conjugate = %f + %fi\n", conjugate);
	return 0;
}
