#include <stdio.h>

enum access {R = 4, W = 2, X = 1};

int main ()
{
	int mod = R + X;
	printf ("%d\n", mod);
	if (mod & R) printf ("readable\n");
	if (mod & W) printf ("writable\n");
	if (mod & X) printf ("executable\n");
	return 0;
}
