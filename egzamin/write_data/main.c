#include <stdio.h>

const int N = 4;

void write_data (const char *filename, int *arr1, float *arr2, char arr3[][2])
{
	FILE *fp = fopen (filename, "a");
	fwrite (arr1, sizeof (int), N, fp);
	fwrite (arr2, sizeof (float), N, fp);
	fwrite (arr3, sizeof (char) * 2, N, fp);
	fclose (fp);
}

int read_data (const char *filename, int *arr1, float *arr2, char arr3[][2])
{
	FILE *fp = fopen (filename, "r");
	fread (arr1, sizeof (int), N, fp);
	fread (arr2, sizeof (float), N, fp);
	fread (arr3, sizeof (char) * 2, N, fp);
	fclose (fp);
}

int main ()
{
	int arr1[] = {1, 2, 3, 4};
	float arr2[] = {0.84, 0.392, 0.783, 0.798};
	char arr3[][2] = {"aa", "bb", "cc", "dd"};
	write_data ("plik.bin", arr1, arr2, arr3);
	int out1[4];
	float out2[4];
	char out3[4][2];
	read_data ("plik.bin", out1, out2, out3);
	float avg1=0, avg2=0;
	for (int i = 0; i < N; i++)
	{
		avg1 += 1.f *out1[i]/N;
		avg2 += out2[i]/N;
	}
	printf ("%f %f\n", avg1, avg2);
	return 0;
}
