#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//str to dynamiczna tablica napisow, n to liczba napisow w niej,
//len to wskaznik za pomoca ktorego zwrocimy dlugosc zwracanej tablicy napisow,
//zwraca dynamiczna tablice napisow ktorej pamiec trzeba zwolnic
char **fun (char **str, int n, int *len)
{
	//Sortowanie bubblesortem ze wzgledu na prefiks
	int prefix1, prefix2;
	char *endptr, tmp[100];
	for (int i = 0; i < n - 1; i++)
		for (int j = 0; j < n - i - 1; j++)
		{	
			prefix1 = strtol (*(str + j), &endptr, 10);
			prefix2 = strtol (*(str + j + 1), &endptr, 10);
			if (prefix1 > prefix2)
			{
				strcpy (tmp, *(str + j));
				strcpy (*(str + j), *(str + j + 1));
				strcpy (*(str + j + 1), tmp);
			}
		}

	//Wyznaczanie ilosci unikalnych prefiksow zeby wiedziec,
	//jak duza tablice napisow utworzyc
	*len = 1;
	for (int i = 1; i < n; i++)
	{
		prefix1 = strtol (*(str + i), &endptr, 10);
		prefix2 = strtol (*(str + i - 1), &endptr, 10);
		if (prefix1 != prefix2)
			(*len)++;
	}

	//Tworzenie dynamicznej tablicy napisow ktora bedziemy zwracac
	char **ret = malloc (sizeof (char *) * *len);
	for (int i = 0; i < *len; i++)
		*(ret + i) = malloc (sizeof (char) * 100);

	//Tworzenie tymczasowej dwuwymiarowej tablicy prefixow i glosow niewaznych
	int **temp = malloc (sizeof (int) * *len);
	for (int i = 0; i < *len; i++)
		*(temp + i) = malloc (sizeof (int) * 2);

	//Sumowanie glosow niewaznych, osobno traktujemy pierwszy napis,
	//zeby potem w petli wiedziec kiedy sie zmienia miasto w zaleznosci
	//od tego, czy prefix z poprzedniego napisu jest rowny obecnemu
	int prefix, votes, valid;
	prefix = strtol (*str, &endptr, 10);
	endptr += 2;
	endptr = strchr (endptr, '>');
	endptr++;
	endptr = strchr (endptr, '>');
	endptr++;
	votes = strtol (endptr, &endptr, 10);
	endptr += 2;
	valid = strtol (endptr, &endptr, 10);
	**temp = prefix;
	*(*temp + 1) = valid ? 0 : votes;
	for (int i = 1, j = 0; i < n; i++)
	{
		prefix2 = strtol (*(str + i - 1), &endptr, 10);
		prefix1 = strtol (*(str + i), &endptr, 10);
		endptr += 2;
		endptr = strchr (endptr, '>');
		endptr++;
		endptr = strchr (endptr, '>');
		endptr++;
		votes = strtol (endptr, &endptr, 10);
		endptr += 2;
		valid = strtol (endptr, &endptr, 10);

		if (prefix2 != prefix1)
			**(temp + ++j) = prefix1;
		*(*(temp + j) + 1) += valid ? 0 : votes;
	}

	//Konwertowanie tablicy liczb do tablicy napisow w wymaganym formacie
	for (int i = 0; i < *len; i++)
	{
		sprintf (*(ret + i), "%d: %d", **(temp + i), *(*(temp + i) + 1));
		free (*(temp + i));
	}
	free (temp);

	//Zwracanie dynamicznie utworzonej tablicy napisow
	return ret;
}

int main ()
{
	//Wczytywanie, utworzenie dynamicznej tablicy napisow
	int n;
	scanf ("%d", &n);
	char **arr = malloc (sizeof (char *) * n);
	for (int i = 0; i < n; i++)
	{
		*(arr + i) = malloc (sizeof (char) * 100);
		scanf ("%s", *(arr + i));
	}
	int len;

	//Wykonanie funkcji
	char **ret = fun (arr, n, &len);

	//Wyswietlenie zwroconej tablicy napisow
	for (int i = 0; i < len; i++)
		printf ("%s\n", *(ret + i));

	//Oproznienie dynamicznie zaalokowanej pamieci
	for (int i = 0; i < len; i++)
		free (*(ret + i));
	free (ret);
	for (int i = 0; i < n; i++)
		free (*(arr + i));
	free (arr);
	return 0;
}
