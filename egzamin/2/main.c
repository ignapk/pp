#include <stdio.h>

char *str_sort (char *str)
{
        for (int i = 0; str[i]; i++)
                if (str[i] > 64 && str[i] < 91)
                        str[i] += 32;
        int len = 0;
        while (*(str + len++));
        len--;
        char tmp;
        for (int i = 0; i < len - 1; i++)
                for (int j = 0; j < len - i - 1; j++)
                        if (*(str + j) > *(str + j + 1))
                        {
                                tmp = *(str + j);
                                *(str + j) = *(str + j + 1);
                                *(str + j + 1) = tmp;
                        }

        return str;
}

void letter_count(char *str)
{
        int count = 0;
        for (int i = 0; str[i]; i++)
        {
                count++;
                if (*(str + i) != *(str + i + 1))
                {
                        printf ("%c %d\n", *(str + i), count);
                        count = 0;
                }
        }
}

int main ()
{
        char str[] = "Practice makes perfect";
        char *ret;
        ret = str_sort (str);
        letter_count (str);
        return 0;
}

