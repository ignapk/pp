#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

void series (int n, float *result)
{
        *result = 0;
        float temp;
        int r;
        for (int a = 1; a <= n; a++)
        {
                if (a & 2)
                {

                        series (a - 1, &temp);
                        *result += ceil (sqrt (temp));
                }
                else
                        *result += ceil (exp (1 + rand () % 2));
        }
}

int main ()
{
        srand (time (0));
        int n;
        scanf ("%d", &n);
        float ret;
        series (n, &ret);
        printf ("%f\n", ret);
        return 0;
}

