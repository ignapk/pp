#include <stdio.h>

void file_hours ()
{
        int h, m, s;
        FILE *fp = fopen ("hours.dat", "r");
        while (fscanf (fp, "%2d:%2d:%2d", &h, &m, &s) != EOF)
                printf ("%.2d:%.2d:%.2d\n", h, m, s);
        return;
}

int main ()
{
        int h, m, s;
        FILE *fp = fopen ("hours.dat", "w");
        while (scanf ("%2d:%2d:%2d", &h, &m, &s))
                fprintf (fp, "%.2d:%.2d:%.2d", h, m, s);
        fclose (fp);
        file_hours ();
        return 0;
}
