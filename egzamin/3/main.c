#include <stdio.h>
#include <math.h>
#include <stdlib.h>

void prism (int x, int y, int z, float ***arr)
{
        for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                        for (int k = 0; k < z; k++)
                                *(*(*(arr + i) + j) + k) = sqrt (pow (i, 2) + pow (j, 2) + pow (k, 2));
}

int main ()
{
        int x, y, z;
        scanf ("%d%d%d", &x, &y, &z);
        float ***arr = malloc (sizeof (float **) * x);
        for (int i = 0; i < x; i++)
        {
                *(arr + i) = malloc (sizeof (float *) * y);
                for (int j = 0; j < y; j++)
                        *(*(arr + i) + j) = malloc (sizeof (float) * z);
        }

        prism (x, y, z, arr);

        for (int k = 0; k < z; k++)
        {
                for (int j = 0; j < y; j++)
                {
                        for (int i = 0; i < x; i++)
                                printf ("%.2f ", *(*(*(arr + i) + j) + k));
                        printf("\n");
                }
                if (k != z - 1)
                        printf ("--------------\n");
        }

        for (int i = 0; i < x; i++)
        {
                for (int j = 0; j < y; j++)
                        free (*(*(arr + i) + j));
                free (*(arr + i));
        }
        free (arr);
        return 0;
}
