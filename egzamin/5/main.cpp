#include <iostream>
#include <string>

enum GameType {RTS, FPS, RPG};

class Player
{
        std::string name;
        int age;
        float score;
        public:
        std::string get_name () { return name; }
        int get_age () { return age; }
        float get_score () { return score; }
        void set_name (std::string n) { name = n; }
        void set_age (int a) { age = a; }
        void set_score (float s) { score = s; }
        Player (std::string n, int a, float s) { name = n; age = a; score = s; }
};

class Game
{
        Player *players[10];
        enum GameType gametype;
        public:
        Game (enum GameType g) { gametype = g; for (int i = 0; i < 10; i++) players[i] = NULL; }
        void addPlayer (Player *p)
        {
                int added = 0;
                for (int i = 0; i < 10; i++)
                        if (players[i] == NULL)
                        {
                                players[i] = p;
                                added = 1;
                                break;
                        }
                if (!added)
                        std::cout << "Couldn't add a new player, maximum of 10 players reached\n";
        }
        void printPlayers (std::string name_part)
        {
                for (int i = 0; i < 10; i++)
                        if (players[i] != NULL && (players[i]->get_name ()).find (name_part) != std::string::npos)
                        {
                                std::string temp = gametype == RTS ? "RTS" : gametype == FPS ? "FPS" : "RPG";
                                std::cout << temp.c_str () << " " << players[i]->get_name ().c_str () << " " << players[i]->get_age() << "\n";
                        }
        }
};

int main ()
{
        Game game = Game (RPG);
        Player *p = new Player ("Ignacy", 22, 100.f);
        Player *q = new Player ("Karol", 22, 100.f);
        Player *r = new Player ("Karystian", 23, 100.f);
        game.addPlayer (p);
        game.addPlayer (q);
        game.addPlayer (r);
        game.printPlayers (std::string("Ka"));
        delete p;
        delete q;
        delete r;
        return 0;
}

