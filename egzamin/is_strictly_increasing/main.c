#include <stdio.h>

int is_strictly_increasing (int *arr, int n)
{
	for (int i = 0; i < n - 1; i++)
		if (*(arr + i) >= *(arr + i + 1))
			return 0;
	return 1;
}

int *fun (int *arr, int n, int (*is_strictly_increasing) (int *, int), int *max)
{
	int *pos;
	*max = 0;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n - i; j++)
			if ((*is_strictly_increasing) (arr + i, n - j - i) && n - j - i > *max)
			{
				*max = n - j - i;
				pos = arr + i;
			}
	return pos;
}

int main ()
{
	int arr[] = {1, 2, 5, 2, 2, 10, 11, 12, 8, -3};
	int l, *pos;
	pos = fun (arr, sizeof (arr) / sizeof (*arr), &is_strictly_increasing, &l);
	for (int i = 0; i < l; i++)
		printf ("%d ", *(pos + i));
	printf ("\n");
	return 0;
}
