#include <stdio.h>

int a;

void f(int b)
{
	a = b;
	return;
}

int main()
{
	scanf("%d", &a);
	f(5);
	printf("%d", a);
	return 0;
}
