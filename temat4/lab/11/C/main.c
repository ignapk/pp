#include <stdio.h>
#include <limits.h>

void fun(int arr[], int n)
{
	for (int i = 0; i < n; i++)
		for (int j = i + 1; j < n; j++)
			if (arr[i] == arr[j])
				arr[j] = INT_MAX;
	for (int i = 0; i < n; i++)
		if (arr[i] != INT_MAX)
			printf("%d\n", arr[i]);
}

void bubblesort(int arr[], int n)
{
	int temp;
	for (int i = 0; i < n - 1; i++)
		for (int j = 0; j < n - i - 1; j++)
			if (arr[j] > arr[j+1])
			{
				temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
	return;
}

void fun2(int arr[], int n)
{
	bubblesort(arr, n);

	for (int i = 0; i < n - 1; i++)
	{
		if (arr[i+1] == arr[i])
			continue;
		printf("%d\n", arr[i]);
	}
	printf("%d\n", arr[n-1]);
	return;
}

int main()
{
	int arr[10] = {2, 5, 2, 4, 3, 3, 10, 1, 1, 1};
	fun(arr, 10);
	return 0;
}
