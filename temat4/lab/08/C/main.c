#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

float montecarlo(int n)
{
	float x, y;
	int counter = 0;
	for(int i = 0; i < n; ++i)
	{
		x = rand() / (float)RAND_MAX;
		y = 1.f * rand() / RAND_MAX;
		if (pow(x - 0.5f, 2) + pow(y - 0.5f, 2) <= 0.5f * 0.5f)
			++counter;
	}
	return 4 * counter / n;
}

int main()
{
	srand(time(0));
	printf("%f", montecarlo(1000));
	return 0;
}
