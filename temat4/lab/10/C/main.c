#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void bubblesort(int arr[], int n)
{
	int temp;
	for (int i = 0; i < n - 1; i++)
		for (int j = 0; j < n - i - 1; j++)
			if (arr[j] > arr[j+1])
			{
				temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
	return;
}

int main()
{
	srand(time(0));
	int arr[10];

	for (int i = 0; i<10; i++)
		arr[i] = rand()%101;

	bubblesort(arr, 10);

	for (int i = 0; i<10; i++)
		printf("%d\n", arr[i]);

	return 0;
}
