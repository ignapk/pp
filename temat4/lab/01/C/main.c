#include <stdio.h>

int main()
{
	int a, b;
	scanf("<%d;%d)", &a, &b);
	for (int i = a; i<b; i++)
		if (i%3 && i%5)
			printf("%d, ", i);
	return 0;
}
