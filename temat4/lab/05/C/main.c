#include <stdio.h>

int nwd(int a, int b)
{
	int temp;
	if (a > b)
		temp = b;
	else
		temp = a;
	do {
		if (a%temp == 0 && b%temp == 0 )
			return temp;
	} while (temp--);
}

int nww(int a, int b)
{
	return (a * b) / nwd(a, b);
}

int main()
{
	int a, b;
	scanf("%d%d", &a, &b);
	printf("%d\n", nwd(a, b));
	printf("%d", nww(a, b));
	return 0;
}
