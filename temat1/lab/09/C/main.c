#include <stdio.h>

int main()
{
	int k;
	float a_k, S_k;
	scanf("%d%f%f", &k, &a_k, &S_k);
	
	float a_1 = S_k * 2.0f / k - a_k;

	printf("Pierwszy wyraz ciągu to: %f", a_1);
	return 0;
}
