#include <stdio.h>

int main()
{
	float a, b;
	scanf("%f%f", &a, &b);
	printf("%f * %f = %f", a, b, a * b);
	return 0;
}
