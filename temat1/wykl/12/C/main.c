#include <stdio.h>
#include <math.h>

int main()
{
	float v;
	const float c = 299792458;
	scanf("%f", &v);
	//printf("%e", v * v) / c * c);
	printf("%e", 1 / sqrt(1 - (v * v) / (c * c)));
	return 0;
}
