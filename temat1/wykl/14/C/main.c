#include <stdio.h>
#include <math.h>

int main()
{
	int rok;
	long int tranz;
	float moore;
	scanf("%i%li", &rok, &tranz);
	moore = 2250 * pow(2, (rok - 1971)/2);
	printf("%f, %.2f%%", moore, 100 * moore / tranz);
	return 0;
}
