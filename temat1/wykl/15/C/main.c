#include <stdio.h>
#include <math.h>

int main()
{
	int rok1, rok2;
	long tranz1, tranz2;
	scanf("%i%li%i%li", &rok1, &tranz1, &rok2, &tranz2);
	printf("%f", 12 * (rok2 - rok1) / log(tranz2 / tranz1) * log(2));
	return 0;
}
