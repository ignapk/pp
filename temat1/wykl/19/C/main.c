#include <stdio.h>

int main()
{
	int HPC, SPT, C, H, S;
	scanf("%i%i%i%i%i", &HPC, &SPT, &C, &H, &S);
	printf("%x", (C * HPC + H) * SPT + (S - 1));
	return 0;
}
