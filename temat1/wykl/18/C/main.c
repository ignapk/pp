#include <stdio.h>

int main()
{
	int h0, dh, m0, dm, s0, ds;
	float udzial_obliczen, sekundy;
	int watki;
	scanf("%i:%i:%i", &dh, &dm, &ds);
	scanf("%i:%i:%i", &h0, &m0, &s0);
	scanf("%f", &udzial_obliczen);

	sekundy = (ds + 60.f * dm + 3600.f * dh) / (1.f - udzial_obliczen / 100.f /*+ udzial_obliczen / 100.f / watki*/);

	printf("%f\n", sekundy);
	printf("%.2i:%.2i:%.2i", (int)(sekundy)/3600, (int)(sekundy/60)%60, (int)(sekundy)%60);
	return 0;
}
