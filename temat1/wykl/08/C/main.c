#include <stdio.h>

int main()
{
	int a, b, c;
	float avg;

	scanf("%d%d%d", &a, &b, &c);
	avg = (a + b + c) / 3.0f; //gdyby nie 3.0f to bylaby zaokraglana do int w dol
	printf("Śr. arytmetyczna: %f\n", avg);
	return 0;
}
