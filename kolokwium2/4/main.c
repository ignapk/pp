#include <stdio.h>
#include <stdlib.h>

const int N = 4;

float fun_aux (float a, float b, float c)
{
	return a > b && a > c ? a : b > c ? b : c;
}

void fun_arr (float *arr, float *out, float (*fun_aux) (float, float, float))
{
	for (int i = 0; i < N; i++)
		*(out + i) = (*fun_aux) (*(arr + 3 * i), *(arr + 3 * i + 1), *(arr + 3 * i + 2));
}

int main ()
{
	float *arr = malloc (sizeof (float) * 3 * N);
	float *out = malloc (sizeof (float) * N);

	for (int i = 0; i < 3 * N; i++)
		scanf ("%f", arr + i);

	fun_arr (arr, out, &fun_aux);

	for (int i = 0; i < N; i++)
		printf ("%f ", *(out + i));
	printf ("\n");

	free (arr);
	free (out);
	return 0;
}
