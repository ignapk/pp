#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void fun(char **list, int n, int xb, int yb)
{
	int x1, x2, y1, y2, dist1, dist2;
	char *name1, *name2, *endptr, temp[100];
	for (int i = 0; i < n - 1; i++)
	{
		for (int j = 0; j < n - i - 1; j++)
		{
			x1 = strtol (*(list + j), &endptr, 10);
			y1 = strtol (endptr + 1, &endptr, 10);
			name1 = endptr + 1;
			x2 = strtol (*(list + j + 1), &endptr, 10);
			y2 = strtol (endptr + 1, &endptr, 10);
			name2 = endptr + 1;
			dist1 = sqrt (pow (x1 - xb, 2) + pow (y1 - yb, 2));
			dist2 = sqrt (pow (x2 - xb, 2) + pow (y2 - yb, 2));
			if (dist1 > dist2 || (dist1 == dist2 && strcmp (name1, name2) > 0))
			{
				strcpy (temp, *(list + j));
				strcpy (*(list + j), *(list + j + 1));
				strcpy (*(list + j + 1), temp);
			}
		}
	}
	for (int i = 0; i < n; i++)
		printf ("%s ", strchr (*(list + i), '#') + 1);
	printf ("\n");
	return;
}

int main ()
{
	int n;
	scanf ("%d", &n);
	char **list = malloc (sizeof (char *) * n);
	for (int i = 0; i < n; i++)
	{
		*(list + i) = malloc (sizeof (char *) * 100);
		scanf ("%s", *(list + i));
	}
	int xb, yb;
	scanf ("%d%d", &xb, &yb);

	fun (list, n, xb, yb);

	for (int i = 0; i < n; i++)
		free (*(list + i));
	free (list);
	return 0;
}
