#include <stdio.h>
#include <string.h>

int fun (void *arr, void *ptr, int n, void *out, size_t size)
{
	int max = 0, current = 0;
	char *last_index;
	for (int i = 0; i < n; i++)
	{
		if (!memcmp (arr + i * size, ptr, size) && max < current)
		{
			max = current;
			current = 0;
			last_index = arr + i * size;
		}
		else
			current++;
	}
	memcpy (out, last_index - max * size, max * size);
	return max;
}

int main ()
{
	int arr[15] = {1, 2, 3, 0, 1, 2, 3, 3, 1, 2, 3, 0, 1, 2, 3};
	int ptr = 0;
	int out[7];
	int ret = fun (arr, &ptr, 15, out, sizeof (int));
	printf ("%d\n", ret);
	for (int i = 0; i < ret; i++)
		printf ("%d ", *(out + i));
	printf ("\n");
	return 0;
}
