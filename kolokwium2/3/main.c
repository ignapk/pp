#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main ()
{
	srand (time (0));
	int n, m = 0;
	scanf ("%d", &n);
	int **arr2d = malloc (sizeof (int *) * n);
	for (int i = 0; i < n; i++)
		if (rand () % 5) 
		{
			*(arr2d + i) = malloc (sizeof (int) * (i + 1));
			for (int j = 0; j < i + 1; j++)
				*(*(arr2d + i) + j) = rand () % 10;
			m++;
		}
		else
		{
			*(arr2d + i) = NULL;
		}
	int **arr2d_2 = malloc (sizeof (int *) * m);
	int *arr = malloc (sizeof (int) * m);
	for (int i = 0, j = 0; i < n; i++)
	{
		if (*(arr2d + i))
		{
			for (int j = 0; j < i + 1; j++)
				printf ("%d ", *(*(arr2d + i) + j));
			printf ("\n");
			*(arr + j) = i + 1;
			*(arr2d_2 + j) = malloc (sizeof (int) * (i + 1));
			memcpy (*(arr2d_2 + j++), *(arr2d + i), sizeof (int) * (i + 1));
		}
		else
			printf ("NULL\n");
	}
	
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < *(arr + i); j++)
			printf ("%d ", *(*(arr2d_2 + i) + j));
		printf ("\n");
		free (*(arr2d_2 + i));
	}
	free (arr2d_2);
	for (int i = 0; i < m; i++)
		printf ("%d ", *(arr + i));
	printf ("\n%d\n", m);
	free (arr);
	for (int i = 0; i < n; i++)
		free (*(arr2d + i));
	free (arr2d);
	return 0;
}
