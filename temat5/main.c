#include <stdio.h>
int f(unsigned int n)
{
	return n ? n*f(n-1) : 1;
}
int main()
{
	int n;
	scanf("%d", &n);
	printf("%d", f(n));
	return 0;
}
