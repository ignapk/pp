#include <stdio.h>
#include <float.h>
#include <math.h>
#include <stdlib.h>

void fun(int arr[], int n, int *ret)
{
	float mean = 0;
	for (int i = 0; i < n; i++) mean+=arr[i];
	mean /= n;

	float diff = fabs(arr[0] - mean);
	int closest;
	for (int i = 0; i < n; i++)
		if ((fabs(arr[i] - mean)) < diff)
		{
			diff = fabs(arr[i] - mean);
			closest = arr[i];
		}
	*ret = closest;
}

int main()
{
	int a[8] = {1, 2, 3, 4, 5, 7, 8, 9};
	int b;
	fun(a,8,&b);
	printf("%d\n", b);
	return 0;
}

