#include <stdio.h>

#define N 5
#define M 10

int fun(int arr[], int n, int m)
{
	int max = arr[0];

	for (int i = 0; i < n; i++)
		for (int j = 1; j < m; j++)
			if (arr[i*m + j] > max) max = arr[i*m + j];

	return max;
}

int main()
{
	int arr[N][M];
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			arr[i][j] = i*j;
	printf("%d\n", fun((int *)arr, N, M));
	return 0;
}
