#include <stdio.h>

static inline int square(int a) { return a*a; }

int main()
{
	int a =2, c;
	c = square(a);
	printf("%d\n", c);
	return 0;
}
