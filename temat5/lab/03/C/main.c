#include <stdio.h>
#include <time.h>

int lcg(int a, int b, int m, int n)
{
	if (n == 1)
		return time(0) % m;
	return (a * lcg(a, b, m, n - 1) + b) % m;
}

int main()
{
	int a = 403, b = 43, m = 201;

	for (int i = 1; i < 200; i++)
		printf("%d\t", lcg(a, b, m, i));
	printf("\n");
	return 0;
}
