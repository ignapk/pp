#include <stdio.h>

unsigned int factorial(unsigned int a)
{
	if (a == 0)
		return 1;
	return a*factorial(a-1);
}

int main()
{
	printf("%u\n", factorial(3));
	return 0;
}
