#include <stdio.h>

unsigned int fib(unsigned int K, int a, int b)
{
	if (K == 1) return a;
	return fib(K - 1, b, a + b);
}

int main()
{
	printf("%u\n", fib(5, 0, 1));
	return 0;
}

