#include <stdio.h>

void fun(int wartosc, int N)
{
	printf("%d\n", wartosc);
	if (wartosc == N)		
		return;
	fun(wartosc+1, N);
}

int main()
{
	int N;
	scanf("%d", &N);
	fun(1, N);
	return 0;
}
