#include <stdio.h>

int T(int k, int x, int a, int b)
{
	if (k==0) return a;
	if (k==1) return b;
	return T(k-1, x, b, 2*x*b-a);
}

int main()
{
	printf("%d\n", T(2, 5, 1, 5));
	return 0;
}
