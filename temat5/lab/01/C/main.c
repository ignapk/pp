#include <stdio.h>

#define square(A) ((A) * (A))

int main()
{
	printf("%d\n", square(3));
	return 0;
}
