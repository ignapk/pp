#include <stdio.h>

void fun (int *begin, int *end)
{
	while ((begin++) - (end + 1)) printf("%d\n", *(begin-1));
}

int main ()
{
	int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	fun(arr, &arr[9]);
	return 0;
}

