#include <stdio.h>

void fun (int *a, int *b, int *c)
{
	*c = *a + *b;
}

int main ()
{
	int a = 1, b = 2, c;
	fun (&a, &b, &c);
	printf ("%d", c);
	return 0;
}
