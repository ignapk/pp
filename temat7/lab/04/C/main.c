#include <stdio.h>

int fun (char *str, int a)
{
	if (!*str) return a;
	return fun (str + 1, a + 1);
}

int main ()
{
	char str[] = "abc";
	printf ("%d", fun(str, 0));
	return 0;
}
