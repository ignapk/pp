#include <stdio.h>

void fun (const int *a, int *const b)
{
	*b = *a;
}

int main ()
{
	const int a = 5;
	int b;
	int *const c = &b;
	fun (&a, c);
	printf ("%d", b);
	return 0;
}	
