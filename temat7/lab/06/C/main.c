#include <stdio.h>
#include <stdbool.h>

void fun (int a, bool *b)
{
	*b = true;
	while (a)
	{
		if (a % 10 & 1)
			*b = false;
		a /= 10;
	}
}

int main ()
{
	int a = 23842;
	bool b;
	fun (a, &b);
	printf ("%d", b);
	return 0;
}	
