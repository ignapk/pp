#include <stdio.h>
#include <stdbool.h>

void swap (int *a, int *b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

void print_arr (int *arr, int n)
{
	for (int i = 0; i < n; i++)
		printf ("%d ", *(arr + i));
	printf ("\n");
}

int *fun (int arr[], int n, bool *b)
{
	int tmp;
	int *even = NULL;
	*b = false;
	for (int i = 0; i < n - 1; i++)
		for (int j = 0; j < n - i - 1; j++)
			if (!(*(arr + j) % 2) && *(arr + j + 1) % 2)
			{
				swap (arr + j, arr + j + 1);
				even = arr + j + 1;
				*b = true;
			}
	return even;
}

int main ()
{
	int arr[7] = {1, 4, 3, 5, 2, 7, 8};
	bool b;
	int *ret = fun (arr, 7, &b);
	print_arr (arr, 7);
	printf ("ret: %d\nb: %d", *ret, b);
	return 0;
}
