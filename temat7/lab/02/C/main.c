#include <stdio.h>

void fun (float *a, float *b)
{
	float temp = *a;
	*a = *b;
	*b = temp;
	return;
}

int main ()
{
	float a = 4, b = 5;
	fun (&a, &b);
	printf ("%f %f", a, b);
}
