#include <stdio.h>

int *fun (int arr[], int n, int v)
{
	for (int i = 0; i < n; i++)
		if (arr[i] == v)
			return &arr[i];
	return NULL;
}

int main ()
{
	int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	int v = 5;
	printf("%d", *(fun(arr, 10, v)));
	return 0;
}
