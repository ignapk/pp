#include <stdio.h>
#include <stdlib.h>

int main ()
{
	int N, M;
	scanf ("%d %d", &N, &M);
	int **field = malloc (sizeof (int *) * N);
	for (int i = 0; i < N; i++)
	{
		*(field + i) = malloc (sizeof (int) * 2 * M);
		for (int j = 0; j < 2 * M; j++)
			scanf ("%d", *(field + i) + j);
	}

	printf ("\n\n");

	int C, ax, ay, dx, dy;
	scanf ("%d", &C);
	for (int i = 0; i < C; i++)
	{
		scanf ("%d %d %d %d", &ax, &ay, &dx, &dy);
		if (*(*(field + ax) + 2 * ay) > *(*(field + dx) + 2 * dy + 1))
			*(*(field + dx) + 2 * dy) = -1;
		else
			*(*(field + ax) + 2 * ay) = -1;
	}



	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < 2 * M; j+=2)
			printf ("%c ", *(*(field + i) + j) == -1 ? 'X' : '0');
		printf ("\n");
	}



	for (int i = 0; i < N; i++)
		free (*(field + i));
	free (field);
	return 0;
}
