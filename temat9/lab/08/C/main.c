#include <stdio.h>
#include <string.h>

int main ()
{
	const int n = 5;
	int a[] = {0, 0, 0, 0, 0}, b[] = {1, 2, 3, 4, 5}; 
	memcpy (a, b, n * sizeof (int));
	memmove (a, a + 2, sizeof (int) * (n - 2));
	memmove (a + n - 1, b + 1, sizeof (int));
	memmove (a + n - 2, b, sizeof (int));
	for (int i = 0; i < n; i++)
		printf ("%d ", *(a + i));
	printf ("\n");
	for (int i = 0; i < n; i++)
		printf ("%d ", *(b + i));
	printf ("\n");
	return 0;
}
