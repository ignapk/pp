#include <stdio.h>
#include <math.h>
#include <stdlib.h>

float *add (float *v, float *u)
{
	float *ret = malloc (sizeof (float) * 3);
	for (int i = 0; i < 3; i++)
		*(ret + i) = *(v + i) + *(u + i);
	return ret;
}

float *sub (float *v, float *u)
{
	float *ret = malloc (sizeof (float) * 3);
	for (int i = 0; i < 3; i++)
		*(ret + i) = *(v + i) - *(u + i);
	return ret;
}

float *mul (float *v, float s)
{
	float *ret = malloc (sizeof (float) * 3);
	for (int i = 0; i < 3; i++)
		*(ret + i) = *(v + i) * s;
	return ret;
}

float muls (float *v, float *u)
{
	float ret = 0.f;
	for (int i = 0; i < 3; i++)
		ret += *(v + i) * *(u + i);
	return ret;
}

float *mulv (float *v, float *u)
{
	float *ret = malloc (sizeof (float) * 3);
	*ret = *(v + 1) * *(u + 2) - *(v + 2) * *(u + 1);
	*(ret + 1) = *(v + 2) * *u - *v * *(u + 2);
	*(ret + 2) = *v * *(u + 1) - *(v + 1) * *u;
	return ret;
}

void print_vector (float *v)
{
	printf ("(%f, %f, %f)\n", *v, *(v + 1), *(v + 2));
}

int main ()
{
	float s = 3.f, v[3] = {1.1f, 2.2f, 3.3f}, u[3] = {1.f, 2.f, 1.f};
	print_vector (v);
	print_vector (u);
	float *sum = add (u, v);
	print_vector (sum);
	float *diff = sub (u, v);
	print_vector (diff);
	float *product = mul (u, s);
	print_vector (product);
	float products = muls (u, v);
	printf ("%f\n", products);
	float *productv = mulv (u, v);
	print_vector (productv);
	free (sum);
	free (diff);
	free (product);
	free (productv);
	return 0;
}
