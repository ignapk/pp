#include <stdio.h>

/*
 * ABC
 * DEF
 * GHI
 * ABC
 * DEF
 */

float fun (float *arr)
{
	return 0.f 
		+ arr[0]*arr[4]*arr[8]
		+ arr[1]*arr[5]*arr[6]
		+ arr[2]*arr[3]*arr[7]
		- arr[2]*arr[4]*arr[6]
		- arr[0]*arr[5]*arr[7]
		- arr[1]*arr[3]*arr[8]
		;
}

int main ()
{
	float arr[] = {5, 4, -7, 1, 3, -1, 2, 0, -3};
	printf ("%f", fun (arr));
	return 0;
}
