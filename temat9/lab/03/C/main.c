#include <stdio.h>
#include <stdlib.h>

int main ()
{
	char *arr = malloc (sizeof (char) * 32);
	for (int i = 0; i < 32 / sizeof (int); i++)
		*((int *)arr + i) = i;
	for (int i = 0; i < 32 / sizeof (int); i++)
	{
		printf ("%d\n", *((int *)arr + i));
	}
	free (arr);
	return 0;
}
