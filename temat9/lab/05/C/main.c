#include <stdio.h>

void fun (float *matrix, int n, int m, float s)
{
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			*(matrix + i * m + j) *= s;
}

int main ()
{
	float matrix[] = {5, 4, -7, 1, 3, -1, 2, 0, -3};
	fun (matrix, 3, 3, 2.4f);

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
			printf ("%.1f\t", *(matrix + 3*i + j));
		printf ("\n");
	}

	return 0;
}
