#include <stdio.h>

char *find_match (char *string, char *match)
{
	int found;
	for (int i = 0; *(string + i); i++)
		if (*(string + i) == *match)
		{
			found = 1;
			for (int j = 1; *(string + i + j) && *(match + j); j++)
				if (*(string + i + j) != *(match + j))
				{
					found = 0;
					break;
				}
			if (found)
				return string + i;
		}
	return NULL;
}

int main ()
{
	char *string = "Ala ma kota.";
	char *match = "ma";
	char *ret = find_match (string, match);
	if (ret)
		printf ("%c", *ret);
	else
		printf ("Brak");
	return 0;
}
