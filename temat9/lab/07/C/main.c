#include <stdio.h>
#include <stdlib.h>

float *mul (float *u, int n, int m, float *v, int o, int p)
{
	if (m != o)
		return NULL;
	float *ret = malloc (sizeof (float) * n * p);
	for (int i = 0; i < n * p; i++)
		*(ret + i) = 0.f;
	for (int i = 0; i < n; i++)
		for (int k = 0; k < p; k++)
			for (int j = 0; j < m; j++)
				*(ret + i * p + k) += *(u + i * m + j) * *(v + j * p + k);
	return ret;
}

void print_matrix (float *u, int n, int m)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
			printf ("%.1f\t", *(u + i * m + j));
		printf ("\n");
	}
}


int main ()
{
	float A[] = {1,3,5,7, 2,4,6,8}; //2x4
	float B[] = {1,8,9, 2,7,10, 3,6,11, 4,5,12}; //4x3
	float *R = mul (A, 2, 4, B, 4, 3);
	print_matrix (R, 2, 3);
	free (R);
	return 0;
}
