#include <stdio.h>

int isprime(int n)
{
	for (int i = 2; i < n; i++)
		if (!(n%i))
			return 0;
	return 1;
}

int main()
{
	int a, b;
	scanf("%d%d", &a, &b);
	for (int i = a; i <= b; i++)
		if(isprime(i)) printf("%d\n", i);
	return 0;
}
