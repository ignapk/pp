#include <stdio.h>

int main ()
{
	float arr[15] = {1.f, 2.f, 3.f, 4.f, 5.f, 6.f, 7.f, 8.f, 9.f, 10.f, 11.f, 12.f, 13.f, 14.f, 15.f};

	int temp, n = 5;

	while (n--)
	{
		scanf ("%d", &temp);
		if (temp < 0 || temp > 14)
			printf ("Nope");
		else
			printf ("%f", arr[temp]);
	}
	return 0;
}
