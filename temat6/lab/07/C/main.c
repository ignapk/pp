#include <stdio.h>

unsigned int fun(char s[], unsigned int n)
{
	unsigned int m = 0;
	for(int i = 0; s[i]; i++)
		m = (m << 1) | (s[i]-48);
	return m + n;
}

int main()
{
	char str[] = "10";
	printf("%d", fun(str, 4));
	return 0;
}
