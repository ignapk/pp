#include <stdio.h>

int main()
{
	int a, mask;
	scanf("%d", &a);
	mask = a >> sizeof(int) * 8 - 1;
	printf("%u\n", (a + mask) ^ mask);
	return 0;
}
