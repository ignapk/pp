#include <stdio.h>

int fun(unsigned int n, int a, int b)
{
	if (n == 0) return a;
	if (n == 1) return b;
	return fun(n-1, b, b + 2 * a + 3);
}
