#include <stdio.h>
#include <math.h>

int fun(float arr[], int n)
{
	for (int i =0; i<ceil(n/2); i++)
		if(arr[i] != arr[n-i-1])
			return 0;
	return 1;
}

int main()
{
	float arr[10];
	for (int i =0; i<10;i++)
		scanf("%f", &arr[i]);
	printf("%d",fun(arr, 10));
	
	return 0;
}
