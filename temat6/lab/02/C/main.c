#include <stdio.h>

int main()
{
	char napis[5];
	scanf("%s", napis);
	for (int i = 0; napis[i]; i++)
		if (napis[i] > 96 && napis[i] < 123)
			napis[i] -= 32;
	printf("%s", napis);
	return 0;
}
