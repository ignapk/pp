#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10
#define M 10

void fun(float arr[][M], int n, int m)
{
	int count;
	float sum;
	float arr_cp[N][M];

	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			arr_cp[i][j] = arr[i][j];

	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
		{
			sum = arr_cp[i][j];
			count = 1;
			if(i < n-1)	sum += arr_cp[i+1][j], count++;
			if(i > 0)	sum += arr_cp[i-1][j], count++;
			if(j < m-1)	sum += arr_cp[i][j+1], count++;
			if(j > 0)	sum += arr_cp[i][j-1], count++;
			arr[i][j] = sum/count;
		}
}

int main()
{
	float arr[N][M];

	srand(time(0));
	for (int i = 0; i<N; i++)
	{
		for (int j = 0; j<M; j++)
		{
			arr[i][j] = rand()%100;
			printf("%5.1f", arr[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	fun(arr, N, M);
	for (int i = 0; i < N; i++)
	{
		for(int j = 0; j<M; j++)	
		{
			printf("%5.1f", arr[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	return 0;
}
