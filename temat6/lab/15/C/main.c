#include <stdio.h>

int fun (int n, int m, int a)
{
	if (n > m) return a;
	if (n == 0 || n == 1) return fun (n + 1, m, 1);
	if (n  % 2 == 0) return fun (n + 1, m, a + n);
	else return fun (n + 1, m, a * n);
}

int a (int n) {
    if (n == 0 || n == 1) return 1;
    if (n % 2 == 0)       return a (n-1) + n;
    else                 return a (n-1) * n;
}

int main ()
{
	for (int i = 0; i < 10; i++)
	{
		printf("fun: %d\na: %d\n", fun(0, i, 1), a(i));
	}	
	return 0;
}
