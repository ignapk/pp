#include <stdio.h>

int fun (int init, int year, int season)
{
	if (year == 0 && season == 0) return init;

	int n = fun (init, year - !season, (season + 3) % 4);

	switch (season)
	{
		case 0:
			return n * 3 / 2;
			break;
		case 1:
			return n;
			break;
		case 2:
			return n > 10 ? n - 10 : 0;
			break;
		case 3:
			return n * 0.6f;
			break;
		default:
			break;
	}
}

int main()
{
	int init = 1000, year = 5, season = 2;
	printf("%d", fun (init, year, season));
	return 0;
}
