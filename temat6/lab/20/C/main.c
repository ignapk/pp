#include <stdio.h>

int main ()
{
	char str[] = "Ala ma kota.";
	for (int i = 0; str[i]; i++)
	{
		if (str[i] == ' ')
		{
			printf ("\n");
			if (str[i+1] > 96 && str[i+1] < 123) str[i+1] -= 32;
		}
		else printf ("%c", str[i]);
	}
	return 0;
}
