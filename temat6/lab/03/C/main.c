#include <stdio.h>
#include <math.h>

int main()
{
	int a, b;
	scanf("%d%d", &a, &b);
	if (a < 0 || b < 0) printf("no");
	else if (b == 0) printf("%f", NAN);
	else printf("%f", sqrt(a / pow(b, 5)));
	return 0;
}

