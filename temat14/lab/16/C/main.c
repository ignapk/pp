#include <stdio.h>
#include <stdlib.h>

int divv (int n)
{
	int ret = 1;
	for (int i = 2; i < n; i++)
		if (!(n % i))
			ret = i;
	return ret;
}

int main ()
{
	int N, D, temp;
	scanf ("%d", &N);
	int *arr;
	for (int i = 0; i < N; i++)
	{
		scanf ("%d", &D);
		arr = malloc (sizeof (int) * D);
		for (int j = 0; j < D; j++)
			scanf ("%d", arr + j);
		for (int j = 0; j < D - 2; j+=2)
		{
			for (int k = 0; k < D - j - 2; k+=2)
			{
				if (*(arr + k) > *(arr + k + 2) || (*(arr + k) == *(arr + k + 2) && divv (*(arr + k + 1)) < divv (*(arr + k + 3))))
				{
					temp = *(arr + k);
					*(arr + k) = *(arr + k + 2);
					*(arr + k + 2) = temp;
					temp = *(arr + k + 1);
					*(arr + k + 1) = *(arr + k + 3);
					*(arr + k + 3) = temp;
				}
			}
		}
		for (int j = 0; j < D; j++)
			printf ("%d ", *(arr + j));
		printf ("\n");
		free (arr);
	}
	return 0;
}
