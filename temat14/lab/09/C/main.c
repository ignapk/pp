#include <stdio.h>

struct car
{
	unsigned char register_number;
	char brand_name[16];
};

void fun (struct car *arr, int n, const char *filename)
{
	FILE *fp = fopen (filename, "w");
	fwrite (arr, sizeof (struct car), n, fp);
	fclose (fp);
}

int main ()
{
	struct car arr[5] = {1, "Volvo", 2, "Opel", 3, "BMW", 4, "Tesla", 5, "Fiat"};
	fun (arr, 5, "plik.txt");
	return 0;
}
