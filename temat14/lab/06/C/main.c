#include <stdio.h>

void fun (const char *path)
{
	FILE *fp = fopen (path, "r");
	int min, max;
	int ret = fscanf(fp, "%d", &min);
	ret = fscanf (fp, "%d", &max);
	if (ret == EOF)
		max = min;
	else
		do
		{
			ret = fscanf (fp, "%d", &max);
		}
		while (ret != EOF);
	printf ("min: %d, max: %d\n", min, max);
}

int main ()
{
	fun ("plik.txt");
	return 0;
}
