#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int comp (const void *a, const void *b)
{
	return *(int *)a - *(int *)b;
}

void fun (const char *filename)
{
	FILE *fp = fopen (filename, "r");
	int count = 0, tmp;
	while (fscanf (fp, "%d", &tmp) != EOF) count++;
	fclose (fp);
	int *arr = malloc (sizeof (int) * (count + 5));
	fp = fopen (filename, "r");
	for (int i = 0; i < count; i++)
		fscanf (fp, "%d", arr + i);
	fclose (fp);
	int min = *arr;
	int max = *(arr + count - 1);
	for (int i = 0; i < 5; i++)
		*(arr + count + i) = min + rand () % (max - min + 1); 
	qsort (arr, count + 5, sizeof (int), &comp);
	fp = fopen (filename, "w");
	for (int i = 0; i < count + 5; i++)
		fprintf (fp, "%d ", *(arr + i));
	fclose (fp);
	free (arr);
}

int main ()
{
	srand (time (0));
	FILE *fp = fopen ("plik.txt", "w");
	int arr[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	for (int i = 0; i < 10; i++)
		fprintf (fp, "%d ", *(arr + i));
	fclose (fp);
	fp = fopen ("plik.txt", "r");
	int tmp;
	while (fscanf (fp, "%d", &tmp) != EOF)
		printf ("%d ", tmp);
	printf ("\n");
	fclose (fp);
	fun ("plik.txt");
	fp = fopen ("plik.txt", "r");
	while (fscanf (fp, "%d", &tmp) != EOF)
		printf ("%d ", tmp);
	printf ("\n");
	fclose (fp);
	return 0;
}
