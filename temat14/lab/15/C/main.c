#include <stdio.h>

int main ()
{
	char name[100];
	int age, height;
	float weight;
	scanf ("%s %d %d %f", &name, &age, &height, &weight);
	printf ("%100s\n%x\n%d\n%.3f\n", name, age, height, weight);
	return 0;
}
