#include <stdio.h>

int main ()
{
	FILE *fp = fopen ("plik.txt", "wt");
	if (!fp)
	{
		perror ("fopen () failed");
		return 1;
	}

	int arr[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	fwrite (arr, sizeof (int), 10, fp);
	fclose (fp);
	return 0;
}
