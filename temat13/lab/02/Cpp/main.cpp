#include <cstdio>
#include <cstring>

enum AnimalType {CAT, DOG};

class Animal
{
	char name[11];
	enum AnimalType type;
	public:
	void say_something (int n)
	{
		switch (type)
		{
			case CAT:
				for (int i = 0; i < n; i++)
					printf ("miał\n");
				break;
			case DOG:
				for (int i = 0; i < n; i++)
					printf ("hał\n");
				break;
			default:
				printf ("???\n");
				break;
		}
	}
	Animal (const char *name, enum AnimalType type)
	{
		strcpy (this->name, name);
		this->type = type;
	}

	Animal (const Animal &animal)
	{
		strcpy (this->name, animal.name);
		this->type = animal.type;
	}

	~Animal ()
	{
		printf ("EXTERMINATE\n");
	}
};

int main ()
{
	Animal *cat = new Animal ("Buffy", CAT), *dog = new Animal ("Xavier", DOG);
	cat->say_something (2);
	dog->say_something (3);
	Animal *copy = new Animal (*cat);
	delete cat;
	delete dog;
	copy->say_something (4);
	delete copy;
	return 0;
}
