#include <cstdio>

class Box
{
	double width, height, depth;
	
	class Ball
	{
		double r;
		public:
		Ball (double r)
		{
			this->r = r;
		}
		int fits (Box *box)
		{
			return r <= box->width/2 && r <= box->height/2 && r <= box->depth/2;
		}
	};
	Ball *ball;
	public:
	Box (double w, double h, double d, double r)
	{
		this->width = w;
		this->height = h;
		this->depth = d;
		ball = new Ball (r);
	}
	
	int ball_fitting (Ball *b)
	{
		return b->fits (this);
	}
	Ball *get_ball () { return ball; }
	~Box () { delete ball; }
};

int main ()
{
	Box *box = new Box (5.0, 6.0, 7.2, 2.5);
	printf ("%d\n", box->ball_fitting (box->get_ball ()));
	delete box;
	return 0;
}
