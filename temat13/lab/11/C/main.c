#include <stdio.h>
#include <stdlib.h>

void calculate_years_leap_days (int *arr, int n, int a, int b, int m)
{
    int tmp, n_leap_days = 0, f_0 = 0, f_1 = 2;
    for (int i = 2; i <= n; ++i)
    {
        tmp = (a * f_1 + f_0 + b) % m;
        f_0 = f_1;
        f_1 = tmp;
        if (f_1 % 2)
            ++n_leap_days;
        arr[i] = n_leap_days;
    }
}
unsigned int calculate_day_of_the_week (int *arr, int *days_months, int day, int month, int year, int leap_month, int n_week_days, int n_months)
{
    int tmp = year - 1, n_leap_days = 0;
    if (year > 1)
        n_leap_days = (month > leap_month && arr[year] > arr[tmp]) ? arr[tmp] + 1 : arr[tmp];
    return (days_months[n_months] * tmp + days_months[month - 1] + day + n_leap_days - 1) % n_week_days + 1;
}

int main ()
{
	int day, month, year, n_months, n_week_days, leap_month, a, b, m;
	scanf ("%d%d%d%d%d%d", &n_months, &n_week_days, &leap_month, &a, &b, &m);

	int *days_months = malloc (sizeof (int) * (n_months + 1));
	for (int i = 1; i <= n_months; i++)
	{
		scanf ("%d", days_months + i);
		days_months[i] += days_months[i - 1];
	}

	scanf ("%d%d%d", &day, &month, &year);
	int *years_n_leap_days = malloc (sizeof (int) * (year + 1));
	calculate_years_leap_days (years_n_leap_days, year, a, b, m);
	
	printf ("%u\n", calculate_day_of_the_week (years_n_leap_days, days_months, day, month, year, leap_month, n_week_days, n_months));

	free (days_months);
	free (years_n_leap_days);
	return 0;
}
