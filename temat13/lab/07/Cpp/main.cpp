#include <cstdio>

class Rectangle
{
	float width;
	float height;
	public:
	Rectangle (float w=5.f, float h=3.f)
	{
		this->width = w;
		this->height = h;
	}
	Rectangle duplicate ()
	{
		return Rectangle (this->width * 2, this->height * 2);
	}
	float area ()
	{
		return width * height;
	}
};

int main ()
{
	Rectangle test = Rectangle ();
	Rectangle d_test = test.duplicate ();
	printf ("%f\n%f\n", test.area (), d_test.area ());
	return 0;
}
