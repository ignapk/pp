#include <cstdio>

class Complex
{
	public:
		double real, imag;
		Complex (double r, double i)
		{
			this->real = r;
			this->imag = i;
		}
};

Complex add (Complex n1, Complex n2)
{
	return Complex(n1.real + n2.real, n1.imag + n2.imag);
}

int main ()
{
	double real, imag;
	scanf ("%lf%lf", &real, &imag);
	Complex n1 = Complex (real, imag);
	scanf ("%lf%lf", &real, &imag);
	Complex n2 = Complex (real, imag);
	Complex ret = add (n1, n2);
	printf ("%lf + %lfi\n", ret.real, ret.imag);
	return 0;
}
