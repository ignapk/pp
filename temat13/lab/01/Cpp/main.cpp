#include <cstdio>
#include <cstring>

enum AnimalType {CAT, DOG};

class Animal
{
	char name[11];
	enum AnimalType type;
	public:
	void say_something (int n)
	{
		switch (type)
		{
			case CAT:
				for (int i = 0; i < n; i++)
					printf ("miał\n");
				break;
			case DOG:
				for (int i = 0; i < n; i++)
					printf ("hał\n");
				break;
			default:
				printf ("???\n");
				break;
		}
	}
	Animal (const char *name, enum AnimalType type);
};

Animal::Animal (const char *name, enum AnimalType type)
{
	strcpy (this->name, name);
	this->type = type;
}

int main ()
{
	Animal *cat = new Animal ("Buffy", CAT), *dog = new Animal ("Xavier", DOG);
	cat->say_something (2);
	dog->say_something (3);
	return 0;
}
