#include <cstdio>
#include <cmath>

struct Point
{
	float x;
	float y;
	Point (float x, float y) : x(x), y(y) {}
};

class Rectangle
{
	public:
	Point **arr;
	float area ()
	{
		float a = sqrt (pow (arr[0]->x - arr[1]->x, 2) + pow (arr[0]->y - arr[1]->y, 2));
		float b = sqrt (pow (arr[0]->x - arr[2]->x, 2) + pow (arr[0]->y - arr[2]->y, 2));
		return a * b;
	}
	Rectangle (float x, float y, float w, float h)
	{
		arr = new Point*[4];
		arr[0] = new Point (x, y);
		arr[1] = new Point (x + w, y);
		arr[2] = new Point (x, y + h);
		arr[3] = new Point (x + w, y + h);
	}
	~Rectangle ()
	{
		for (int i = 0; i < 4; i++)
			delete arr[i];
		delete[] arr;
	}
};

int main ()
{
	Rectangle *rect = new Rectangle (1, 2, 5, 4);
	printf ("%f\n", rect->area ());
	delete rect;
	return 0;
}
