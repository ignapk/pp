#include <cstdio>
#include <cmath>

struct Point
{
	float x;
	float y;
	Point (float x, float y) : x(x), y(y) {}
};

class Rectangle
{
	public:
	Point **arr;
	Rectangle& operator =(const Rectangle &rect)
	{
		if (this->arr)
		{
			for (int i = 0; i < 4; i++)
				delete arr[i];
			delete[] arr;
		}
		this->arr = new Point*[4];
		for (int i = 0; i < 4; i++)
			this->arr[i] = new Point(rect.arr[i]->x, rect.arr[i]->y);
		return *this;
	}

	float area ()
	{
		float a = sqrt (pow (arr[0]->x - arr[1]->x, 2) + pow (arr[0]->y - arr[1]->y, 2));
		float b = sqrt (pow (arr[0]->x - arr[2]->x, 2) + pow (arr[0]->y - arr[2]->y, 2));
		return a * b;
	}
	
	Rectangle () { arr = NULL; }

	Rectangle (float x, float y, float w, float h)
	{
		arr = new Point*[4];
		arr[0] = new Point (x, y);
		arr[1] = new Point (x + w, y);
		arr[2] = new Point (x, y + h);
		arr[3] = new Point (x + w, y + h);
	}
	Rectangle (const Rectangle &rect)
	{
		this->arr = new Point*[4];
		for (int i = 0; i < 4; i++)
			this->arr[i] = new Point (rect.arr[i]->x, rect.arr[i]->y);
	}

	~Rectangle ()
	{
		for (int i = 0; i < 4; i++)
			delete arr[i];
		delete[] arr;
	}
};

int main ()
{
	Rectangle *rect = new Rectangle (1, 2, 5, 4);
	Rectangle *rect_cpy = new Rectangle (*rect);
	Rectangle rect_cpy_2;
	rect_cpy_2 = *rect;
	printf ("%f\n", rect->area ());
	printf ("%f\n", rect_cpy_2.area ());
	delete rect;
	printf ("%f\n", rect_cpy->area ());
	printf ("%f\n", rect_cpy_2.area ());
	delete rect_cpy;
	return 0;
}
