#include <cstdio>
#include <cstring>
#include <cmath>

class Student
{
	public:
		char name[100];
		int result_1, result_2, result_3;
		Student () {}
		Student (const char *name, int r1, int r2, int r3=0)
		{
			strcpy (this->name, name);
			this->result_1 = r1;
			this->result_2 = r2;
			this->result_3 = r3;
		}
};

float avg (Student *arr, int n)
{
	float ret = 0.f;
	Student temp;
	for (int i = 0; i < n; i++)
	{
		temp = *(arr + i);
		if (temp.result_1 + temp.result_2 <= 50)
		{
			if ((temp.result_1 < temp.result_2 ? temp.result_2 : temp.result_1) + temp.result_3 <= 50)
			{
				ret += 2;
				continue;
			}
			else
			{
				ret += 3;
				continue;
			}
		}
		else
		{
			ret += (floor((temp.result_1 + temp.result_2 - 51)/10) + 6)/2;
			continue;
		}
	}
	return ret / n;
}

int main ()
{
	Student arr[3];
	arr[0] = Student ("Jan Nowak", 40, 4, 30);
	arr[1] = Student ("Adam Kowalski", 26, 30);
	arr[2] = Student ("Adam Kowalski", 50, 50);
	printf ("%f\n", avg (arr, 3));
	return 0;
}
