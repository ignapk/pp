#include <cstdio>
#include <cstdlib>

#define SIZE 10

class MyQueue
{
    int *arr, capacity, front, rear, count;

public:
    MyQueue (int size = SIZE);
    ~MyQueue ();

    void pop ();
    void push (int x);
    int first ();
    int size ();
    bool is_empty ();
    bool is_full ();
};

MyQueue::MyQueue (int size)
{
    arr = new int[size];
    capacity = size;
    front = 0;
    rear = -1;
    count = 0;
}

MyQueue::~MyQueue ()
{
    delete[] arr;
}

void MyQueue::pop ()
{
    if (is_empty ())
    {
        exit (EXIT_FAILURE); //Zakończenie programu błędem
    }
    front = (front + 1) % capacity;
    count--;
}

void MyQueue::push (int item)
{
    if (is_full ())
    {
        exit (EXIT_FAILURE); //Zakończenie programu błędem
    }
    rear = (rear + 1) % capacity;
    arr[rear] = item;
    count++;
}

int MyQueue::first ()
{
    if (is_empty ())
    {
        exit (EXIT_FAILURE); //Zakończenie programu błędem
    }
    return arr[front];
}

int MyQueue::size ()
{
    return count;
}

bool MyQueue::is_empty ()
{
    return (size () == 0);
}

bool MyQueue::is_full ()
{
    return (size () == capacity);
}

int main ()
{
    MyQueue q (5);

    q.push (1);
    q.push (2);
    q.push (3);

    printf ("Pierwszy element: %i\n", q.first ());
    
    q.pop ();
    q.push (4);
    
    printf ("Rozmiar kolejki: %i\n", q.size ());
    q.pop ();
    q.pop ();
    q.pop ();
    
    if (q.is_empty ()) 
        printf ("Kolejka jest pusta.\n");
    else 
        printf ("Kolejka nie jest pusta.\n");

    return 0;
}
