#include <cstdio>

class Rect
{
	int x, y;
	public:
	int get_area ()
	{
		return x * y;
	}
	Rect (int x, int y)
	{
		this->x = x;
		this->y = y;
	}
};

int main ()
{
	int x, y;
	scanf ("%d%d", &x, &y);
	Rect rect = Rect (x, y);
	printf ("%d", rect.get_area ());
	return 0;
}
