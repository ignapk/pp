#include <stdio.h>
#include <stdlib.h>

//returns pointer to first element of array of floats that needs to be freed
float *transpose (float *A, int n)
{
	float *ret = malloc (sizeof (float) * n * n);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			*(ret + i * n + j) = *(A + j * n + i);
	return ret;
}

int main ()
{
	int n = 4;
	float *A = malloc (sizeof (float) * n * n);
	for (int i = 0; i < n * n; i++)
		*(A + i) = i;
	float *AT = transpose (A, n);
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			printf ("%f\t", *(AT + i * n + j));
		printf ("\n");
	}
	free (A);
	free (AT);
	return 0;
}
