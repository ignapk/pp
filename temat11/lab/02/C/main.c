#include <stdio.h>
#include <stdlib.h>

int comp (const void *a, const void *b)
{
	return *(int *)a - *(int *)b;
}

void fun (int *arr, int n, int k, int *result)
{
	qsort (arr, n, sizeof (int), &comp);
	for (int i = 0, j = 0; i < n - 1 && j < k; i++)
		if (*(arr + n - i - 1) != *(arr + n - i - 2) || i == n - 2)
				*(result + j++) = *(arr + n - i - 1);
}

int main ()
{
	int arr[] = {4, 5, 9, 12, 9, 45, 22, 45, 7};
	int n = sizeof (arr) / sizeof (*arr);
	int k = 3;
	int result[k];
	fun (arr, n, k, result);
	for (int i = 0; i < k; i++)
		printf ("%d ", *(result + i));
	printf ("\n");
	return 0;
}
