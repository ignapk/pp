#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

void fun (unsigned int n, int *arr, float *result)
{
	*result = 0.f;
	for (int i = 0; i <= n; i++)
		*result += *(arr + i) % 5 ? sqrtf (i) : pow (i, 3);
}

int main ()
{
	srand (time (0));
	unsigned int n;
	scanf ("%u", &n);
	int *arr = malloc (sizeof (int) * n);
	for (int i = 0; i < n; i++)
		*(arr + i) = rand () % 16;
	float ret;
	fun (n, arr, &ret);
	printf ("%f\n", ret);
	free (arr);
	return 0;
}
