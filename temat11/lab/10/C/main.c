#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

int main ()
{
	int *a = malloc (sizeof (int) * 10);
	int *b = malloc (sizeof (int) * 10);
	int x;
	scanf ("%d", &x);
	wmemset (a, x, 10);
	wmemset (b, x + 2, 10);
	for (int i = 0; i < 10; i++)
		printf ("%d ", *(a + i));
	printf ("\n");
	for (int i = 0; i < 10; i++)
		printf ("%d ", *(b + i));
	free (a);
	free (b);
	return 0;
}
