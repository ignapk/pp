#include <stdio.h>

void fun (void *ptr, int ptrsize)
{
	if (ptrsize - 1)
		(*(int *)ptr)++;
	else
		(*(char *)ptr)++;
}

int main ()
{
	char a = 'a';
	int b = 5;
	fun (&a, sizeof (a));
	fun (&b, sizeof (b));
	printf ("%c%i\n", a, b);
	return 0;
}
