#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main ()
{
	int arr[] = {0, 1, 3, 4, 5, 6, 7, 8, 10};
	int n = sizeof (arr) / sizeof (*arr);
	printf ("%d\n", n);
	/*
	int temp;
	for (int i = 0; i < n; i+=2)
	{
		temp = *(arr + n - 1);
		memmove (arr + i + 1, arr + i, sizeof (int) * (n - i - 1));
		*(arr + i) = temp;
	}
	*/
	int *min = arr;
	int *max = arr + n - 1;
	int *temp = malloc (sizeof (int) * n);
	for (int i = 0; i < n; i++)
		if (i%2)
			*(temp + i) = *min++;
		else
			*(temp + i) = *max--;
	memcpy (arr, temp, sizeof (int) * n);
	free (temp);
	for (int i = 0; i < n; i++)
		printf ("%d ", *(arr + i));
	printf ("\n");
	return 0;
}
