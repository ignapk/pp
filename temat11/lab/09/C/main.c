#include <stdio.h>
#include <stdlib.h>

int comp (const void *a, const void *b)
{
	return *(int *)a - *(int *)b;
}

void fun (int *arr, int n, int *min, int *max)
{
	qsort (arr, n, sizeof (int), &comp);
	*min = *arr;
	*max = *arr;
	for (int i = 0; i < n - 1; i++)
		if (*(arr + i) != *(arr + i + 1))
		{
			*max = *(arr + i + 1);
			break;
		}
}

int main ()
{
	int arr[] = {5, 6, 7, 2, 3, 4, 12};
	int n = sizeof (arr) / sizeof (*arr);
	int min, max;
	fun (arr, n, &min, &max);
	printf ("%d%d", min, max);
	return 0;
}
