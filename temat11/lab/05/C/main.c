#include <stdio.h>
#include <stdlib.h>

void add (int *a, int *b)
{
	*a += *b;
	return;
}

void sub (int *a, int *b)
{
	*a -= *b;
	return;
}

void f (int *A, int *B, int N, void (*oper) (int *, int *))
{
	for (int i = 0; i < N*N; i++)
		(*oper) (A + i, B + i);
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
			printf ("%d\t", *(A + i * N + j));
		printf ("\n");
	}
	return;
}

int main ()
{
	int A[] = {1, 2, 3, 5, 15, 0, 12, 13, 5};
	int B[] = {1, 2, 3, 5, 15, 0, 12, 13, 5};
	int N = 3;
	f (A, B, N, &add);
	f (A, B, N, &sub);
	f (A, B, N, &sub);
	return 0;
}
