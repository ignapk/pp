#include <stdio.h>
#include <wchar.h>

int *fun (int *arr, int n, int value)
{
	return wmemchr (arr, value, n);
}

int main ()
{
	int arr[] = {1, 2, 3};
	int *ret = fun (arr, 3, 2);
	if (ret)
		printf ("%i", *ret);
	else
		printf ("Ds");
	return 0;
}
