#include <stdio.h>
#include <stdlib.h>

int comp (const void *a, const void *b)
{
	return *(int *)a - *(int *)b;
}

int fun (int *arr, int n)
{
	//int ret = *arr;
	//for (int i = 1; i < n; i++)
	//ret = ^= *(arr + i);
	//return ret;
	qsort (arr, n, sizeof (int), &comp);
	if (*arr != *(arr + 1)) return *arr;
	else if (*(arr + n - 2) != *(arr + n - 1)) return *(arr + n - 1);
	else
		for (int i = 1; i < n - 1; i++)
			if (*(arr + i - 1) != *(arr + i) && *(arr + i) != *(arr + i + 1))
				return *(arr + i);
}

int main ()
{
	int arr[] = {3, 3, 5, 1, 5, 7, 1, 9, 9};
	int n = sizeof (arr) / sizeof (*arr);
	printf ("%d\n", fun (arr, n));
	return 0;
}
