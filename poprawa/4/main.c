#include <stdio.h>

int s (int a)
{
	if (a + 10 > 224)
		return 224;
	else
		return a + 10;
}

int main ()
{
	FILE *input = fopen ("addresses.dat", "r");
	FILE *output = fopen ("addresses_modified.dat", "w");
	int a, b, c, d;
	while (fscanf (input, "%d.%d.%d.%d\n", &a, &b, &c, &d) != EOF)
		fprintf (output, "%d.%d.%d.%d\n", s (a), s (b), s (c), s (d));
	fclose (input);
	fclose (output);
	return 0;
}
