#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int bits (unsigned int a)
{
	int count = 0;
	for (int i = 0; a >> i; i++)
		if ((a >> i) & 1)
			count++;
	return count;
}

int max_bits (unsigned int *arr, int n)
{
	int max = 0;
	int ret;
	for (int i = 0; i < n; i++)
		if (bits (*(arr + i)) > max)
		{
			max = bits (*(arr + i));
			ret = i;
		}
	return ret;
}

int main ()
{
	srand (time (0));
	int n, a, b;
	scanf ("%d %d %d", &n, &a, &b);
	unsigned int *arr = malloc (sizeof (unsigned int) * n);
	for (int i = 0; i < n; i++)
		*(arr + i) = rand () % (b - a + 1) + a;
	printf ("%d", *(arr + max_bits (arr, n)));
	free (arr);
	return 0;
}
