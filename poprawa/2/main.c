#include <stdio.h>

void letters (char *str, char *vow, int *vpos, char *con, int *cpos)
{
	int found_vowel = 0;
	*vpos = -1;
	*cpos = -1;
	for (int i = 0; *(str + i); i++)
	{
		switch (*(str + i))
		{
			case ' ':
				continue;
				break;
			case 'a':
			case 'A':
			case 'e':
			case 'E':
			case 'i':
			case 'I':
			case 'o':
			case 'O':
			case 'u':
			case 'U':
			case 'y':
			case 'Y':
				if (!found_vowel)
				{
					*vow = *(str + i);
					*vpos = i;
					found_vowel = 1;
				}
				break;
			default:
				*con = *(str + i);
				*cpos = i;
				break;
		}
	}
}

int main ()
{
	char str[] = "Practice makes perfect";
	char vow, con;
	int vpos, cpos;
	letters (str, &vow, &vpos, &con, &cpos);
	printf ("%d:%c %d:%c", vpos, vow, cpos, con);
	return 0;
}
