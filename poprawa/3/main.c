#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int comp (const void *a, const void *b)
{
	if (*(float *)a < *(float *)b) return -1;
	return *(float *)a > *(float *)b;
}

void mat_sort (float **arr, int w, int h)
{
	for (int i = 0; i < w; i++)
		qsort (*(arr + i), h, sizeof (float), &comp);
}

int main ()
{
	srand (time (0));
	int w, h;
	scanf ("%d %d", &w, &h);
	float **arr = malloc (sizeof (float *) * w);
	for (int i = 0; i < w; i++)
	{
		*(arr + i) = malloc (sizeof (float) * h);
		for (int j = 0; j < h; j++)
		{
			*(*(arr + i) + j) = 1.f * rand () / RAND_MAX;
			printf ("%f ", *(*(arr + i) + j));
		}
		printf ("\n");
	}
	mat_sort (arr, w, h);
	printf ("------------------\n");
	for (int i = 0; i < w; i++)
	{
		for (int j = 0; j < h; j++)
			printf ("%f ", *(*(arr + i) + j));
		printf ("\n");
		free (*(arr + i));
	}
	free (arr);
	return 0;
}
