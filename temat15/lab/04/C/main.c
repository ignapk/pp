#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main ()
{
	char *str = NULL;
	size_t len;
	getline (&str, &len, stdin);
	printf ("%d\n", strstr (str, "dowolne") - str);
	free (str);
	return 0;
}
