#include <stdio.h>

int main ()
{
	char str[100];
	scanf ("%s", str);
	int ret = 0;
	for (int i = 0; str[i]; i++)
		ret = ret * 2 + str[i] - '0';
	printf ("%d\n", ret);
	return 0;
}
