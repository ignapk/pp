#include <stdio.h>
#include <string.h>

struct numerals
{
	int integer;
	const char *numeral;
};

const struct numerals numerals_map[] =
{
	1000, "M",
	900, "CM",
	500, "D",
	400, "CD",
	100, "C",
	90, "XC",
	50, "L",
	40, "XL",
	10, "X",
	9, "IX",
	5, "V",
	4, "IV",
	1, "I",
	0, NULL
};

int main ()
{
	int n;
	scanf ("%d", &n);
	char ret[100] = "";
	for (const struct numerals *curr = numerals_map; curr->integer > 0; curr++)
	{
		while (n >= curr->integer)
		{
			strcat (ret, curr->numeral);
			n -= curr->integer;
		}
	}
	printf ("%s\n", ret);
}
